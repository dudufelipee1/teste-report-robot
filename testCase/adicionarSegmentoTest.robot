[Documentation]     Testes do CRUD de Segmento no ambiente de teste. link: https://sistemadecreditoteste.hml.cloud.poupex/segmento-cliente

*** Settings ***
Resource                  ../resources/global.robot
Resource                  ../testData/service/adicionarSegmentoService.robot
Resource                  ../testData/service/loginService.robot
Test Setup                Preparar Suite
Test Teardown             Terminar Suite

*** Test Case ***
CT001 - Incluir segmentos de clientes
    [Documentation]    Critério de aceite: Permitir a inclusão de segmentos de clientes .
    [Tags]             incluir_segmento
    Given que efetuei o Login
    When acesso a funcionalidade de inclusão de segmentos de clientes
    And informo os dados obrigatórios para a inclusão e salvo
    Then o sistema registra a inclusão de segmento de cliente

CT002 - Editar segmentos de clientes
    [Documentation]    Critério de aceite: Permitir a edição de segmentos de clientes ​
    [Tags]             editar_segmento
    Given que efetuei o Login
    When acesso a funcionalidade de alteração de segmentos de clientes
    And altero os dados obrigatórios desejados    
    Then o sistema registra a inclusão de segmento de cliente

CT003 - Consultar segmentos de clientes 
    [Documentation]    Critério de aceite: Permitir a consulta de segmentos de clientes ​
    [Tags]             consultar_segmento
    Given que efetuei o Login
    When acesso a funcionalidade de consulta de segmentos de clientes
    And informo os parâmetros para consulta
    Then o sistema apresenta os segmentos de acordo com os parâmetros informados

CT004 - Excluir segmentos de clientes 
    [Documentation]    Critério de aceite: Permitir a exclusão de segmentos de clientes ​
    [Tags]             excluir_segmento
    Given que efetuei o Login
    And existe segmentos de clientes na base de dados
    When acesso a funcionalidade de exclusão de segmentos de clientes
    And confirmo a exclusão segmento
    Then o sistema exclui o segmento selecionado

    