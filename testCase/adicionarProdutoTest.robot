[Documentation]     Testes do CRUD de Produto no ambiente de teste. link: https://sistemadecreditoteste.hml.cloud.poupex/produto

*** Settings ***
Resource                  ../resources/global.robot
Resource                  ../testData/service/adicionarProdutoService.robot
Resource                  ../testData/service/loginService.robot
Library                   SeleniumLibrary
Test Setup                Preparar Suite
Test Teardown             Terminar Suite

*** Test Case ***
CT001 - Incluir Produtos 
    [Documentation]     Critério de aceite: Permitir a inclusão de produtos ​
    [Tags]              incluir_produto            
    Given que efetuei o Login 
    When o usuário acessar a funcionalidade de inclusão de produtos
    And informar os dados obrigatórios para a inclusão
    And salvar a inclusão
    Then o sistema registra a inclusão do produto 
    And apresenta a mensagem "Produto adicionado com sucesso!"    

CT002 - Validar Campos Obrigatórios 
    [Documentation]        Critério de aceite: Permitir a validação do preenchimento dos campos obrigatórios ​
    [Tags]                 validar_campos
    Given que o usuário está incluindo um produto
    When o usuário não informar os dados obrigatórios para a inclusão
    Then o sistema apresenta a mensagem "Campo Obrigatório" para os campos obrigatórios não preenchidos

CT003 - Valor Máximo maior que o Valor Mínimo
    [Documentation]        Critério de aceite: Validar se o valor máximo é maior que o valor mínimo ​
    [Tags]                 validar_valor
    Given que o usuário está incluindo um produto
    When o usuário informar o valor máximo menor que o valor mínimo
    Then o sistema apresenta a mensagem "Valor máximo deve ser maior que o valor mínimo."

CT004 - Mês final maior que o mês inicial 
    [Documentation]        Critério de aceite: Validar se o mês final é maior que o mês inicial ​
    [Tags]                 validar_mes
    Given que o usuário está incluindo uma taxa de juros de um produto
    When o usuário informar o mês final menor que o mês inicial
    Then o sistema apresenta a mensagem "Mês final deve ser maior que o mês inicial.".

CT005 - Exclusão de Produto 
    [Documentation]        Critério de aceite: Efetuar a exclusão do Produto ​
    [Tags]                 excluir_produto
    Given que usuário esteja logado no sistema 
    When usuário acessar o Produto que deseja excluir        
    Then sistema exclui Produto
    