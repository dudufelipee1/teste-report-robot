[Documentation]           Testes dos CRUD de Carteira no ambiente de Teste. link: https://sistemadecreditoteste.hml.cloud.poupex/carteira

*** Settings ***
Resource                  ../resources/global.robot
Resource                  ../testData/service/adicionarCarteiraService.robot
Resource                  ../testData/service/loginService.robot
Test Setup                Preparar Suite
Test Teardown             Terminar Suite

*** Test Case ***
CT001 - Adicionar Carteira
    [Documentation]     O usuário irá efetuar a inclusão de carteira
    [tags]              inclusao_carteira
    Given que efetuei o Login    
    When acesso a funcionalidade de inclusão de carteiras
    And informo os dados obrigatórios para a inclusão
    And salvo a inclusão
    Then o sistema registra a inclusão de carteira
    
    
CT002 - Editar Carteira
    [Documentation]     O usuário irá efetuar a edição da carteira
    [tags]              edicao_carteira
    Given que efetuei o Login
    And acessar menu carteira
    And existem carteiras na base de dados
    When acesso a funcionalidade de alteração de carteira
    And altero os dados obrigatórios desejados
    And Salvo a Alteração
    Then o sistema registra a alteração da carteira selecionada
    
CT003 - Consultar Carteira
    [Documentation]     Permitir a consulta de carteira.
    [tags]              consulta_carteira
    Given que efetuei o Login
    When acesso o menu de Carteiras
    And existem carteiras na base de dados    
    And informo os parâmetros para consulta
    Then o sistema apresenta as carteiras de acordo com os parâmetros informados

CT004 - Exclusão da Carteira
    [Documentation]     Permitir a exclusao da carteira
    [tags]              excluir_carteira
    Given que efetuei o Login
    And acessar menu carteira
    And existem carteiras na base de dados
    When acesso a funcionalidade de exclusão de carteiras
    And confirmo a exclusão   
    Then o sistema exclui a carteira selecionada
    
