*** Settings ***
Resource                  database.robot
Library                   SeleniumLibrary    
Library                   DateTime    
Library                   OperatingSystem   
Library                   String

*** Variables ***
${browser}                chrome
${URL}                    https://sistemadecreditoteste.hml.cloud.poupex/segmento-cliente 
*** Keywords ***
Preparar Suite
    Open Browser                   ${URL}    ${browser}
    Maximize Browser Window   
Terminar Suite
    Capture Page Screenshot
    Close Browser    
    
Validar Title
    [Arguments]                    ${title} 
    Title Should Be                ${title}
    
Validar URL
    [Arguments]                    ${url} 
    Location Should Contain        ${url}

Validar URL Completa
    [Arguments]                    ${URL_ESPERADA} 
    ${URL_ATUAL}=                  Get Location
    Should Be Equal                ${URL_ATUAL}  ${URL_ESPERADA} 
       
Descer Barra de Rolagem
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)

Descer Barra Rolagem
    Execute Javascript    $("html, body").animate({scrollTop:500},900);

Subir Barra de Rolagem
    Execute Javascript    window.scrollTo(0, document.body.scrollHeight)

Diminuir Zoom Tela
    Execute Javascript      document.body.style.zoom="80%"


    
