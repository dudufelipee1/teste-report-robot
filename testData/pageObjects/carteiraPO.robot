*** Settings ***
Resource                  ../../resources/global.robot

*** Variable ***

${menuCarteira}                 //a[@href='/carteira']
${campoNome}                    id:nome
${nomeCarteira}                 Carteira Auto  # Alterar com o nome da carteira na lista
${listaCarteira}                //a[contains(text(),'${nomeCarteira}')]
${botaoFiltrar}                 xpath=//button[normalize-space(text()) = 'Filtrar']
${botaoLimpar}                  xpath=//button[@title = 'Limpar']
${botaoAdicionarCarteira}       //a[@href='/carteira/nova']

