*** Settings ***
Resource        ../../resources/database.robot
Resource        ../../resources/global.robot
Library         SeleniumLibrary

*** Variable ***

${menuSegmentoClientes}            xpath=//span[contains(text(),'Segmento de Cliente')]
${campoNome}                       xpath=//input[@id= 'nome']
${campocdgSegmento}                xpath=//input[@id= 'codigoBDC']
${campoDiaVencimento}              xpath=//input[@id= 'diaPrimeiroVencimento']
${botaoFiltrar}                    xpath=//button[normalize-space(text()) = 'Filtrar']
${botaoLimpar}                     xpath=//button[@title = 'Limpar']
${botaoAdicionarSegmento}          xpath=//a[(contains(text(), 'Adicionar segmento de cliente' ))]
${menuConfig}                      xpath=//i[@class= 'fas fa-fw fa-cogs']