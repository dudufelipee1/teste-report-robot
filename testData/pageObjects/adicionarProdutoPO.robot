*** Settings ***
Resource                  ../../resources/global.robot
Resource                  loginPO.robot
Resource                  ../dao/adicionarProdutoDAO.robot
*** Variable ***
${campoNome}                    xpath=//input[@id='nome']
${campoCdgProdutoBdc}           xpath=//input[@id='codigoBDC']
${campoCarteira}                xpath=//input[@id='carteira']
${nomeCarteira}                 Nova carteira Teste   # Alterar nome carteira conforme Teste
${listaOpcaoCarteira}           xpath=//span[normalize-space(text())='${nomeCarteira}']
${campoCarencia}                xpath=//input[@id='carencia']
${campoIdadePrazo}              xpath=//input[@id='idadePrazo']
${campoValorMinimo}             xpath=//input[@id='valorMinimo']
${campoValorMaximo}             xpath=//input[@id='valorMaximo']
${campoAtivo}                   xpath=//input[@id='ativo']
${opcaoAtivo}                   Sim    # ALTERAR DE ACORDO COM O TESTE
${listaOpcaoAtivo}              xpath=//span[normalize-space(text()) = '${opcaoAtivo}']

${botaoAdicionarSegmento}       xpath=//button[@class='btn btn-success']
${listaSegmento}                xpath=//input[@id='segmentoCliente']
${nomeSegmento}                 BANCO DO BRASIL  #  ALTERAR NOME DE ACORDO COM O TESTE   
${selecionarSegmento}           xpath=//input[@id='segmentoCliente']
${campoMargem}                  xpath=//input[@id='percentualMargem']
${campoLimiteContratos}         xpath=//input[@id='limiteContratos']
${campoValorMinimoSegmento}     xpath=(//input[@id='valorMinimo'])[2]
${campoValorMaximoSegmento}     xpath=(//input[@id='valorMaximo'])[2]
${campoPrazoMinimoSegmento}     xpath=//input[@id='prazoMinimo']
${campoPrazoMaximoSegmento}     xpath=//input[@id='prazoMaximo']
${nomeTaxaJuros}                Padrão Militar        # ALTERAR DE ACORDO COM O TESTE
${campoTaxaJuros}               xpath=//input[@id='grupoFaixaJuros']
${excluirSegmento}              xpath=//i[@class='fas fa-trash-alt']

${botaoSalvar}                  xpath=//button[normalize-space(text()) = 'Salvar']
${botaoExcluir}                 xpath=//button[@id='excluirButton']
${opcaoExcluirProduto}          Sim        # --- ALTERAR DE ACORDO COM O TESTE ---
${botaoOpcaoExcluirProduto}     xpath=//button[contains(text(),'${opcaoExcluirProduto}')]    
${botaoEditar}                  xpath=//button[@id='editarButton']

${campoVoltarListaProduto}      xpath=//a[normalize-space(text()) = 'Voltar para lista de produtos']
${validarCadastroProduto}       xpath=//a[normalize-space(text()) = '${nomeProduto}']
${validarExclusaoProduto}       xpath=//i[@class = 'fas fa-fw fa-check-circle text-success']
${msgCampoObrigatorio}          xpath=(//span[contains(text(), 'Campo obrigatório.')])