*** Settings ***
Resource                  ../../resources/global.robot


*** Variable ***

${campoNome}                    id:nome
${nomeCarteira}                 teste
${ativoLista}                   id:ativo
${opcaoAtivo}                   Não     #Alterar a opção desejada para o teste
${listaAtivoOpcao}              xpath=//span[contains(text(),'${opcaoAtivo}')]
${botaoSalvar}                  xpath=//button[@class = 'btn btn-success ng-star-inserted']
${botaoExcluir}                 xpath=//button[@class = 'btn btn-danger float-right ng-star-inserted']
${opcaoExclusao}                Sim     #Alterar de acordo com o teste
${confirmarExclusao}            xpath=//button[contains(text(),'${opcaoExclusao}')]
${validarExclusao}              xpath=//i[@class = 'fas fa-fw fa-check-circle text-success']
${campoVoltarListaCarteira}     xpath=//a[@class = 'btn btn-link pl-0 pr-0']
${validarAlteracao}             xpath=//i[@class = 'fas fa-fw fa-check-circle text-success']
${consultarCarteira}            xpath=//a[normalize-space(text()) = '${nomeCarteira}']

