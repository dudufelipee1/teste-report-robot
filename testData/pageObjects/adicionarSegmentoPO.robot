*** Settings ***
Resource                  ../../resources/global.robot
Resource                  loginPO.robot
Resource                  ../dao/adicionarSegmentoDAO.robot
*** Variable ***
${nomeForm}                     xpath=//input[@id='nome']
${cdgSegmentoBdcForm}           xpath=//input[@id='codigoBDC']
${diaPrimeiroVencimentoForm}    xpath=//input[@id='diaPrimeiroVencimento']
${ativoCbb}                     xpath=//input[@id='ativo']
${opcaoAtivo}                   Sim  # Alterar a opção desejada para o teste
${ativoOptList}                 xpath=//span[contains(text(),'${opcaoAtivo}')]
${btnSalvar}                    xpath=//button[(contains(text(), 'Salvar' ))]
${voltarListaSegmentoElement}   xpath=//a[(contains(text(), 'Voltar para lista de segmentos de clientes' ))]
${msgValidacaoSegmento}         xpath=//p[contains(text(), 'Segmento de Cliente adicionado com sucesso.')]
${msgValidacaoEditarSegmento}   xpath=//p[contains(text(), 'Segmento de cliente salvo com sucesso.')]
${msgValidacaoExcluirSegmento}  xpath=//p[contains(text(), 'Segmento de Cliente excluído com sucesso.')]
${btnEditar}                    xpath=//button[contains(text(),'Editar')]
${btnExcluir}                   xpath=//button[contains(text(),'Excluir')]
${confirmarExclusao}            xpath=//button[contains(text(),'Sim')]