*** Settings ***
Resource                  ../../resources/global.robot


*** Variable ***
${campoNome}                    xpath=//input[@id='nome']
${campoCodigoBdc}               xpath=//input[@id='codigoBDC']
${campoCarteira}                xpath=//input[@id='carteira']
${listaOpçãoCarteira}           xpath=//div[@class= 'ng-dropdown-panel-items scroll-host']
${campoCarencia}                xpath=//input[@id='carencia']
${campoIdadePrazo}              xpath=//input[@id='idadePrazo']
${campoAtivo}                   xpath=//input[@id='ativo']
${opcaoAtivo}                   Sim  #----------REVER ----------
${listaOpcaoAtivo}              xpath=//span[contains(text(), '${opcaoAtivo}' )]
${botaoFiltrar}                 xpath=//button[@class = 'btn btn-info' ]
${botaoLimpar}                  xpath=//button[@title = 'Limpar']
${botaoAdicionarProduto}        xpath=//a[normalize-space(text()) = 'Adicionar produto']
${acessoMenu}                   xpath=//i[@class= 'fas fa-fw fa-cogs']
${menuProduto}                  xpath=//span[contains(text(),'Produto')]
${validarMensagemInclusao}      xpath=//i[@class = 'fas fa-fw fa-check-circle text-success']
${duploClickCarteira}           xpath=//span[@class='ng-arrow-wrapper'][1]
${menuListaProduto}             xpath=(//i[@class= 'fas fa-ellipsis-v'])[2]
${nomeProdutoLista}             Produto B     # ---- Alterar nome de acordo com o teste -----                    
${produtoLista}                 xpath=//a[contains(text(),'${nomeProdutoLista}')]