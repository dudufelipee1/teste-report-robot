*** Settings ***
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/adicionarSegmentoPO.robot
Resource                  ../pageObjects/segmentoClientePO.robot
Resource                  LoginDAO.robot

*** Variable ***
${nomeSegmento}                    Segmento A    
${cdgSegmentoBdc}                  583
${diaPrimeiroVencimento}           1
${selecionarSegmento}              xpath=//a[contains(text(), '${nomeSegmento}')]

*** Keywords ***
acessar inclusão de segmentos
    Descer Barra de Rolagem
    Click Element                ${botaoAdicionarSegmento}

inserir dados inclusão segmento
    Input Text                    ${nomeForm}                      ${nomeSegmento}                     
    Input Text                    ${cdgSegmentoBdcForm}            ${cdgSegmentoBdc}           
    Input Text                    ${diaPrimeiroVencimentoForm}     ${diaPrimeiroVencimento}         
    Click Element                 ${ativoCbb}                               
    Click Element                 ${ativoOptList}           
    Click Element                 ${btnSalvar}

validar inclusão segmento
    Sleep                          1
    ${validarInclusao}=            Run Keyword And Return Status    Element Should Be Visible   ${msgValidacaoSegmento}
    ${validarEdicao}=              Run Keyword And Return Status    Element Should Be Visible   ${msgValidacaoEditarSegmento}
    Run Keyword If                 ${validarInclusao}=='true'       Element Should Be Visible   ${msgValidacaoSegmento}

acessar edição segmento
    Input Text                    ${campoNome}   ${nomeSegmento}
    Click Element                 ${botaoFiltrar}
    Sleep                         2
    Click Element                 ${selecionarSegmento}
    Sleep                         2
    Click Element                 ${btnEditar}

alterar dados do segmento
    Input Text                    ${nomeForm}                      Segmento ABC                     
    Input Text                    ${cdgSegmentoBdcForm}            777           
    Input Text                    ${diaPrimeiroVencimentoForm}     27        
    Click Element                 ${btnSalvar}

acessar menu segmentos
    Click Element                 ${menuConfig}
    Sleep                         5
    Click Element                 ${menuSegmentoClientes}
    
acessar consulta segmento
    Input Text                    ${campoNome}   ${nomeSegmento}
    Click Element                 ${botaoFiltrar}

validar consulta segmento
    Sleep                          3        
    Page Should Contain            Segmento ABC          # Validação baseado na Keyword 'alterar dados do segmento'

acessar exclusao segmento
    Click Element                 ${btnExcluir}

confirmar exclusão segmento
    Click Element                 ${confirmarExclusao}

validar exclusao segmento    
    Wait Until Element Is Visible  ${msgValidacaoExcluirSegmento} 
