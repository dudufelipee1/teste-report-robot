[Documentation]     Escrever o passo a passo e então passar para o BDD na pasta Service
*** Settings ***
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/adicionarCarteiraPO.robot
Resource                  ../pageObjects/produtoPO.robot
Resource                  LoginDAO.robot


*** Keywords ***
Acessar menu Produto
    Click Element           ${acessoMenu}
    Sleep                   2
    Click Element           ${menuProduto}
    Sleep                   2

acessar produto da lista
    Click Element    ${produtoLista}
