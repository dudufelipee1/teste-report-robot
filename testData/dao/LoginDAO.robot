[Documentation]     Escrever o passo a passo e então passar para o BDD na pasta Service
*** Settings ***
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/loginPO.robot

*** Variable ***

*** Keywords ***
Efetuar Login
    Go To                   ${URL}
    Sleep                   5
    Input Text              ${campoCpf}  ${cpfLogin}    
    Click Button            ${botaoLogin}
    Sleep                   5
    Page Should Contain     Segmentos de Clientes
    