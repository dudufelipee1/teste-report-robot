[Documentation]     Escrever o passo a passo e então passar para o BDD na pasta Service
*** Settings ***
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/produtoPO.robot
Resource                  ../pageObjects/adicionarProdutoPO.robot
Resource                  LoginDAO.robot

*** Variable ***

${nomeProduto}             Produto B 
${codigoProdutoBDC}        333 
${carteira}
${carencia}                30
${idadePrazo}              80
${valorMaximo}             10000 
${valorMinimo}             1000
${valorMargem}             1
${valorLimiteContrato}     1
${valorMinimoSegmento}     1000
${valorMaximoSegmento}     10000
${prazoMinimoSegmento}     30
${prazoMaximoSegmento}     60   

                    

*** Keywords ***
Acessar inclusão de produtos
    Click Element           ${acessoMenu}
    Sleep                   2
    Click Element           ${menuProduto}
    Sleep                   2
    Click Element           ${botaoAdicionarProduto}
    
Dados inclusão Produto
    Input Text              ${campoNome}                ${nomeProduto}
    Sleep                   2
    Input Text              ${campoCdgProdutoBdc}       ${codigoProdutoBdc}
    Click Element           ${campoCarteira}
    Sleep                   2
    Click Element           ${listaOpcaoCarteira}
    Input Text              ${campoCarencia}            ${carencia}
    Input Text              ${campoIdadeprazo}          ${idadePrazo}
    Input Text              ${campoValorMinimo}         ${valorMinimo}
    Input Text              ${campoValormaximo}         ${valorMaximo}
    Click Element           ${campoAtivo}
    Click Element           ${listaOpcaoAtivo}
    Click Element           ${botaoAdicionarSegmento}
    Click Element           ${listaSegmento}
    Sleep                   1
    Input Text              ${selecionarSegmento}           ${nomeSegmento}
    Input Text              ${campoMargem}                  ${valorMargem}
    Input Text              ${campoLimitecontratos}         ${valorLimiteContrato}
    Input Text              ${campoValorMinimoSegmento}     ${valorMinimoSegmento}
    Input Text              ${campoValorMaximoSegmento}     ${valorMaximoSegmento}
    Input Text              ${campoPrazoMinimoSegmento}     ${prazoMinimoSegmento}
    Input Text              ${campoPrazoMaximoSegmento}     ${prazoMaximoSegmento}
    Input Text              ${campoTaxaJuros}               ${nomeTaxaJuros}    
    Sleep                   3

Salvar inclusão Produto
    Descer Barra de Rolagem
    Click Element                      ${botaoSalvar}
    Sleep                              2
    
Validar inclusão carteira na lista
    Page Should Contain                ${nomeProduto}      
    
Validar mensagem de inclusão de produto
    Page Should Contain Element        ${validarMensagemInclusao}

Clicar nos campos
    Sleep                              3
    Press Keys                         ${campoNome}                   TAB
    Press Keys                         ${campoCdgProdutoBdc}          TAB
    Press Keys                         ${campoCarteira}               TAB
    Press Keys                         ${campoCarencia}               TAB
    Press Keys                         ${campoIdadePrazo}             TAB
    Press Keys                         ${campoValorMinimo}            TAB
    Press Keys                         ${campoValorMaximo}            TAB
    Press Keys                         ${campoAtivo}                  TAB
    Click Element                      ${botaoAdicionarSegmento} 
    Press Keys                         ${listaSegmento}               TAB
    Press Keys                         ${campoMargem}                 TAB
    Press Keys                         ${campoLimiteContratos}        TAB
    Press Keys                         ${campoValorMinimoSegmento}    TAB
    Press Keys                         ${campoValorMaximoSegmento}    TAB
    Press Keys                         ${campoPrazoMinimoSegmento}    TAB
    Press Keys                         ${campoPrazoMaximoSegmento}    TAB            
    Press Keys                         ${campoTaxaJuros}              TAB
    Capture Page Screenshot
    Subir Barra de Rolagem 
    

Validar campos Obrigatórios
    Sleep                     3    
    FOR    ${i}    IN RANGE    16
        IF    ${i} == 0
            Log    ${i}
        ELSE IF    ${i} == 8
            Log    ${i}
        ELSE            
        Element Should Be Visible    ${msgCampoObrigatorio}\[${i}]     
        END     
    END   

Preencher valor minimo e valor maximo
    Input Text                       ${campoValorMinimo}         ${valorMaximo}
    Input Text                       ${campoValormaximo}         ${valorMinimo}

Validar valor minimo e valor maximo
    Sleep                            1
    Page Should Contain              Valor máximo deve ser maior que o valor mínimo.

Validar mensagem mês final deve ser maior
    Page Should Contain              O valor deve ser maior que ${prazoMaximoSegmento}.

Excluir produto
    Descer Barra de Rolagem
    Sleep                            2
    Click Element                    ${botaoExcluir}    
    Wait Until Element Is Visible    ${botaoOpcaoExcluirProduto}
    Click Element                    ${botaoOpcaoExcluirProduto}

Validar Exclusão produto
    Page Should Contain Element      ${validarExclusaoProduto}
    Page Should Not Contain          ${nomeProduto}

Inserir Dados Mês final maior que o mês inicial
    Input Text                       ${campoNome}                ${nomeProduto}
    Sleep                            2
    Input Text                       ${campoCdgProdutoBdc}       ${codigoProdutoBdc}
    Click Element                    ${campoCarteira}
    Sleep                            2
    Click Element                    ${listaOpcaoCarteira}
    Input Text                       ${campoCarencia}            ${carencia}
    Input Text                       ${campoIdadeprazo}          ${idadePrazo}
    Input Text                       ${campoValorMinimo}         ${valorMinimo}
    Input Text                       ${campoValormaximo}         ${valorMaximo}
    Click Element                    ${campoAtivo}
    Click Element                    ${listaOpcaoAtivo}
    Click Element                    ${botaoAdicionarSegmento}
    Click Element                    ${listaSegmento}
    Sleep                            1
    Input Text                       ${selecionarSegmento}           ${nomeSegmento}
    Input Text                       ${campoMargem}                  ${valorMargem}
    Input Text                       ${campoLimitecontratos}         ${valorLimiteContrato}
    Input Text                       ${campoValorMinimoSegmento}     ${valorMinimoSegmento}
    Input Text                       ${campoValorMaximoSegmento}     ${valorMaximoSegmento}
    Input Text                       ${campoPrazoMinimoSegmento}     ${prazoMaximoSegmento}    # Teste sendo feito aqui
    Input Text                       ${campoPrazoMaximoSegmento}     ${prazoMinimoSegmento}    # Teste sendo feito aqui
    Input Text                       ${campoTaxaJuros}               ${nomeTaxaJuros}    
    Sleep                            3