[Documentation]     Escrever o passo a passo e então passar para o BDD na pasta Service
*** Settings ***
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/carteiraPO.robot
Resource                  ../pageObjects/adicionarCarteiraPO.robot
Resource                  LoginDAO.robot


*** Keywords ***
##QUANDO
Acessar inclusão de carteira
    Click Element                    ${menuCarteira}
    Sleep                            5       
    Click Element                    ${botaoAdicionarCarteira}
    Sleep                            5
    
acessar alterar carteira
    Click Element                   ${listaCarteira}
    
acessar exclusão carteira
    Click Element                   ${consultarCarteira}
    Sleep                           3
    Click Element                   ${botaoExcluir}
    Sleep                           3
##E
Preencher dados para inclusão de carteira    
    Input Text                       ${campoNome}  ${nomeCarteira}
    Click Element                    ${ativoLista}
    Wait Until Element Is Visible    ${listaAtivoOpcao}
    Click Element                    ${listaAtivoOpcao}
    
Salvar Carteira
    Click Button                     ${botaoSalvar}
    Sleep                            5
    
acessar menu carteira
    Click Element                   ${menuCarteira}
    Sleep                           5

consultar carteira
    Sleep                           5
    Page Should Contain             ${nomeCarteira}

alterar dados
    Click Element                   ${ativoLista}
    Click Element                   ${listaAtivoOpcao}

validar alteração carteira
    Page Should Contain Element     ${validarAlteracao}

pesquisar carteira
    Input Text                       ${campoNome}  ${nomeCarteira}
    Click Element                    ${botaoFiltrar}

confirmar exclusao
    Click Element                   ${confirmarExclusao} 
    Sleep                           3
    
##ENTAO
Verificar se carteira foi cadastrada
    Page Should Contain              ${nomeCarteira}
    Sleep                            5

validar exclusao
    Page Should Contain Element      ${validarExclusao}

