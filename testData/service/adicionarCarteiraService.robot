[Documentation]     Escrever o BDD nas Keywords
*** Settings ***
Library                   SeleniumLibrary
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/adicionarCarteiraPO.robot
Resource                  ../dao/adicionarCarteiraDAO.robot
Resource                  loginService.robot              

*** Keywords ***
##QUANDO
acesso a funcionalidade de inclusão de carteiras
    Acessar inclusão de carteira
    
acesso a funcionalidade de alteração de carteira
    Click Element        ${listaCarteira}
    Sleep                5
    
acesso a funcionalidade de exclusão de carteiras
    acessar exclusão carteira
    
##E
informo os dados obrigatórios para a inclusão
    Preencher dados para inclusão de carteira
    
altero os dados obrigatórios desejados
    Click Element        ${ativoLista}
    Click Element        ${listaAtivoOpcao}  # Alterar para opcao desejada

salvo a inclusão
    Salvar Carteira
    
acesso o menu de Carteiras
    acessar menu carteira

Salvo a Alteração
    Salvar Carteira

existem carteiras na base de dados
    consultar carteira

informo os parâmetros para consulta
    pesquisar carteira

confirmo a exclusão
    confirmar exclusao

##ENTAO
o sistema registra a inclusão de carteira
    Verificar se carteira foi cadastrada

o sistema registra a alteração da carteira selecionada
    validar alteração carteira
    
o sistema apresenta as carteiras de acordo com os parâmetros informados
    Verificar se carteira foi cadastrada

o sistema exclui a carteira selecionada
    validar exclusao











    