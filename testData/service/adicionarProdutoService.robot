[Documentation]     Escrever o BDD nas Keywords
*** Settings ***
Library                   SeleniumLibrary
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/adicionarProdutoPO.robot
Resource                  ../dao/adicionarProdutoDAO.robot
Resource                  ../dao/produtoDAO.robot
Resource                  loginService.robot              
Resource                  ../dao/produtoDAO.robot


*** Keywords ***
##DADO
que o usuário está incluindo um produto
    Efetuar Login
    Acessar inclusão de produtos

que o usuário está incluindo uma taxa de juros de um produto
    Efetuar Login
    Acessar inclusão de produtos

que usuário esteja logado no sistema 
    Efetuar Login
    Acessar menu Produto
##QUANDO
o usuário acessar a funcionalidade de inclusão de produtos
    Acessar Inclusão de produtos 

o usuário não informar os dados obrigatórios para a inclusão       
    Clicar nos campos

o usuário informar o valor máximo menor que o valor mínimo
    Preencher valor minimo e valor maximo

o usuário informar o mês final menor que o mês inicial
    Inserir Dados Mês final maior que o mês inicial

usuário acessar o Produto que deseja excluir
    acessar produto da lista
    
    
##E
informar os dados obrigatórios para a inclusão
    Dados inclusão Produto
    
salvar a inclusão
    salvar inclusão Produto

apresenta a mensagem "Produto adicionado com sucesso!"
    Validar inclusão carteira na lista

tenha acessado a listagem de Produto
    Acessar menu Produto
    
##ENTAO
o sistema registra a inclusão do produto
    Validar mensagem de inclusão de produto

o sistema apresenta a mensagem "Campo Obrigatório" para os campos obrigatórios não preenchidos
    Validar campos Obrigatórios

o sistema apresenta a mensagem "Valor máximo deve ser maior que o valor mínimo."
    Validar valor minimo e valor maximo

o sistema apresenta a mensagem "Mês final deve ser maior que o mês inicial.".
    Validar mensagem mês final deve ser maior

sistema exclui Produto
    Excluir produto
    Validar Exclusão produto    





    