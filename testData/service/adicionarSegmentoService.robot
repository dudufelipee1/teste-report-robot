*** Settings ***
Library                   SeleniumLibrary
Resource                  ../../resources/global.robot
Resource                  ../pageObjects/adicionarSegmentoPO.robot
Resource                  ../dao/adicionarSegmentoDAO.robot
Resource                  loginService.robot    

*** Keywords ***
#QUANDO
acesso a funcionalidade de inclusão de segmentos de clientes
    acessar inclusão de segmentos

acesso a funcionalidade de alteração de segmentos de clientes
    acessar edição segmento

#E
informo os dados obrigatórios para a inclusão e salvo
    inserir dados inclusão segmento

altero os dados obrigatórios desejados
    alterar dados do segmento

acesso a funcionalidade de consulta de segmentos de clientes
    acessar menu segmentos

informo os parâmetros para consulta
    acessar consulta segmento

#ENTAO
o sistema registra a inclusão de segmento de cliente
    validar inclusão segmento

o sistema apresenta os segmentos de acordo com os parâmetros informados
    validar consulta segmento

seleciono segmento que desejo excluir
    acessar edição segmento

existe segmentos de clientes na base de dados
    acessar edição segmento

acesso a funcionalidade de exclusão de segmentos de clientes
    acessar exclusao segmento

confirmo a exclusão segmento
    confirmar exclusão segmento

o sistema exclui o segmento selecionado
    validar exclusao segmento





